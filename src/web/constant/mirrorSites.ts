export const mirrorSites: Array<{ origin: string, provider: string, technology: string }> = [
  {
    origin: 'https://wt.bgme.me',
    provider: 'Shrink',
    technology: '自行构建',
  },
  {
    origin: 'https://rbq.desi',
    provider: 'kookxiang',
    technology: '代理',
  },
  {
    origin: 'https://wt.makai.city',
    provider: '友人♪b',
    technology: '自行构建'
  },
  {
    origin: 'https://wt.0w0.bid',
    provider: '立音喵',
    technology: 'Cloudflare Workers',
  },
  {
    origin: 'https://wt.umwings.com',
    provider: 'C86',
    technology: '同步构建产物',
  },
];
